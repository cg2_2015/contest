class Vector {
public:
   float x;
   float y;
};

class Cell {
public:
	float radius;
	Vector position;
	Vector velocity;
};

class ICellAI {
public:

	virtual ~ICellAI();
	
	/// The application will invoke this method once before running the game
	///
	virtual void prepare();

	/// Determine in which direction your cell will accelerate 
	///	according to other cells in the arena.
	/// 
	/// @param cells - An array of cells' information. 
	///	Enemy cells are distance sorted according to your player cell. 
	///	You are cell 0 since you are the closest to yourself. 
	/// @param cellCount - The number of cells alive in the arena.
	/// @return The desired acceleration direction. 
	///	This vector will get normalized before application. 
	///	Could be a zero vector - no acceleration will be applied to your cell 
	///	and your velocity remains the same.
	virtual Vector calculateAcceleration(
		const Cell * cells, 
		const int cellCount
	) = 0;
	
};

