/*
	Copyright (C) 2014 Chaos Software

	Distributed under the Boost Software License, Version 1.0.
	See accompanying file LICENSE_1_0.txt or copy at
	http://www.boost.org/LICENSE_1_0.txt.
*/

// Standard headers
#include <algorithm> // std::sort()

// Project headers
#include "cell.h"
#include "cell_ai.h"
#include "settings.h"
#include "simulator.h"
#include "math_utils.h"
#include "player_cell_info.h"

using namespace std;
using namespace chaos;

////////////////////////////////////////////////////////////
// Helper classes

class DistanceToPlayerComparator {

public:
	DistanceToPlayerComparator(const Cell &inputPlayerCell) : playerCell(inputPlayerCell) {
	}
	
	bool operator()(const Cell &lhs, const Cell &rhs) {
		float playerDistanceToLeftCell = chaos::distance(playerCell, lhs);
		float playerDistanceToRightCell = chaos::distance(playerCell, rhs);
		return playerDistanceToLeftCell < playerDistanceToRightCell;
	}

private:
	const Cell &playerCell;
};

////////////////////////////////////////////////////////////
// Simulator implementation

Simulator::Simulator() 
	: settings()
	, state(NOT_READY)
	, cells(NULL)
	, playerViewCells(NULL)
	, liveCellCount(0)
	, playerCellInfo(NULL)
	, livePlayerCellCount(0) {
}

Simulator::~Simulator() {
	if (cells) {
		delete [] cells;
		cells = NULL;
		liveCellCount = 0;
	}

	if (playerViewCells) {
		delete [] playerViewCells;
		playerViewCells = NULL;
	}

	if (playerCellInfo) {
		delete [] playerCellInfo;
		playerCellInfo = NULL;
		livePlayerCellCount = 0;
	}
}

Simulator::State Simulator::getState() const {
	return state;
}

bool Simulator::isFinished() const {
	return (state == VICTORY) || (state == FAILURE);
}

int Simulator::getLiveCellCount() const {
	return liveCellCount;
}

const Cell* Simulator::getCells() const {
	return cells;
}

int Simulator::getLivePlayerCellCount() const {
	return livePlayerCellCount;
}

const PlayerCellInfo* Simulator::getPlayerCellInfo() const {
	return playerCellInfo;
}

void Simulator::toggleSimulationPause() {
	if (state == PAUSED) {
		state = READY;
	} else if (state == READY) {
		state = PAUSED;
	}
}

void Simulator::populate() {
	// Reset simulator
	state = NOT_READY;

	// Seed the random generator
	unsigned short Xi[3] = { 0,0,0};
	Xi[0] = settings.levelSeed;
	Xi[1] = 2;
	Xi[2] = settings.levelSeed * 12 + 90;
	// Allocate cells
	if (0 < settings.cellCount) {
		if (cells == NULL) {
			cells = new Cell[settings.cellCount];
		}

		if (playerViewCells == NULL) {
			playerViewCells = new Cell[settings.cellCount];
		}
		
		liveCellCount = settings.cellCount;
	}

	// Allocate player cell info
	if (0 < settings.playerCellCount) {
		if (playerCellInfo == NULL) {
			playerCellInfo = new PlayerCellInfo[settings.playerCellCount];
		}

		livePlayerCellCount = 0;
	}

	// Place players
	if (settings.playerCellCount == 1) {
		// Single player
		cells[0].radius = settings.playerCellInitialRadius;
		cells[0].position = settings.arenaCenter;
	} else {
		// Multiplayer
		int placedPlayerCells = 0;
		while (placedPlayerCells < settings.playerCellCount) {
			// Place new player
			Cell &newCell = cells[placedPlayerCells];
			newCell.radius = settings.playerCellInitialRadius;
			newCell.position = settings.arenaCenter + getDiskSample(settings.arenaRadius - settings.playerCellInitialRadius,Xi);
	
			// Check if the new player is OK with the other players
			bool isOkWithOtherCells = true;
			for (int placedPlayerIndex = 0; placedPlayerIndex < placedPlayerCells; ++ placedPlayerIndex) {
				const Cell &placedPlayer = cells[placedPlayerIndex];
				if (areCellsColliding(newCell, placedPlayer)) {
					isOkWithOtherCells = false;
					break;
				}
			}

			// Move to the next cell to place
			if (isOkWithOtherCells) {
				++placedPlayerCells;
			}
		}
	}

	// Place cells
	int placedCells = settings.playerCellCount;
	while (placedCells < liveCellCount) {
		// Place new cell
		Cell &newCell = cells[placedCells];
		newCell.radius = getFloatSample(settings.cellMinimumRadius, settings.cellMaximumRadius, Xi);
		newCell.position = settings.arenaCenter + getDiskSample(settings.arenaRadius - newCell.radius, Xi);
		newCell.velocity = settings.cellMaximumVelocity * getRandomDirection(Xi);

		// Check if the new cell is OK with the other players and cells
		bool isOkWithOtherCells = true;
		for (int placedCellIndex = 0; placedCellIndex < placedCells; ++ placedCellIndex) {
			const Cell &placedCell = cells[placedCellIndex];
			if (areCellsColliding(newCell, placedCell)) {
				isOkWithOtherCells = false;
				break;
			}
		}

		// Move to the next cell to place
		if (isOkWithOtherCells) {
			++placedCells;
		}
	}

	state = READY;
}

void Simulator::addPlayerCell(const ICellAI *cellAI, const float r, const float g, const float b) {
	if (state == READY && livePlayerCellCount < settings.playerCellCount) {
		PlayerCellInfo &playerInfo = playerCellInfo[livePlayerCellCount];
		playerInfo.cellIndex = livePlayerCellCount;
		playerInfo.ai = cellAI;
		playerInfo.r = r;
		playerInfo.g = g;
		playerInfo.b = b;

		++livePlayerCellCount;
	}
}

void Simulator::tick() {
	// 0. Accelerate player cells
	for (int playerIndex = 0; playerIndex < livePlayerCellCount; ++playerIndex) {
		const PlayerCellInfo &playerInfo = playerCellInfo[playerIndex];
		Cell &playerCell = cells[playerInfo.cellIndex];

		acceleratePlayerCell(playerCell, playerInfo.ai);
	}

	// 1. Advance all cells and resolve cell-arena collisions
	for (int cellIndex = 0; cellIndex < liveCellCount; ++cellIndex) {
		Cell &cell = cells[cellIndex];

		// Advance cell
		advanceCell(cell);

		// Collide with arena walls
		collideCellWithArena(cell);
	}

	// 2. Resolve cell-cell collisions
	for (int firstCellIndex = 0; firstCellIndex < liveCellCount; ++firstCellIndex) {
		Cell &firstCell = cells[firstCellIndex];

		for (int secondCellIndex = firstCellIndex + 1; secondCellIndex < liveCellCount; ++secondCellIndex) {
			Cell &secondCell = cells[secondCellIndex];

			// Collide with another cell
			collideCells(firstCell, secondCell);

			if (firstCell.isDead()) {
				defragmentCellPartitions(firstCellIndex);
				--firstCellIndex;
				break;
			}
			
			if (secondCell.isDead()) {
				defragmentCellPartitions(secondCellIndex);
				--secondCellIndex;
				continue;
			}
		}
	}

	// 3. Finish simulation if all players are dead.
	if (livePlayerCellCount == 0) {
		state = FAILURE;
		return;
	}

	// 4. Finish simulation if there is one player standing.
	if (liveCellCount == 1) {
		//printf("%f %f %f ", playerCellInfo[0].r, playerCellInfo[0].g, playerCellInfo[0].b);
		state = VICTORY;
		return;
	}
}

void Simulator::acceleratePlayerCell(Cell &playerCell, const ICellAI *playerCellAI) {
	if (playerCellAI) {
		memcpy(playerViewCells, cells, sizeof(Cell) * liveCellCount);

		sort(playerViewCells, playerViewCells + liveCellCount, DistanceToPlayerComparator(playerCell));

		Vector acceleration = playerCellAI->calculateAcceleration(playerViewCells, liveCellCount);
		acceleration.normalize();

		playerCell.velocity += settings.tickLength * acceleration;
	}
}

void Simulator::advanceCell(Cell &cell) {
	cell.position += cell.velocity * settings.tickLength;
}

bool Simulator::isCellCollidingWithArena(const Cell &cell) const {
	bool result = false;

	float distanceFromArenaCenter = distance(cell.position, settings.arenaCenter) + cell.radius;
	if (settings.arenaRadius < distanceFromArenaCenter) {
		result = true;
	}

	return result;
}

void Simulator::collideCellWithArena(Cell &cell) {
	if (isCellCollidingWithArena(cell)) {
		// Find the unit vector pointing away from the arena at the point of collision.
		Vector normal = cell.position - settings.arenaCenter;
		normal.normalize();

		// Pull the cell back into the arena, making sure it doesn't collide during the next tick.
		cell.position = settings.arenaCenter + (settings.arenaRadius - cell.radius - EPSILON) * normal;

		// Flip the normal. Now it points directly to the arena's center at the point of collision.
		normal = -normal;

		// Use the normal to reflect the cell's velocity.
		cell.velocity -= 2.0f * dot(normal, cell.velocity) * normal;
	}
}

bool Simulator::areCellsColliding(const Cell &lhs, const Cell &rhs) const {
	bool result = false;

	if (distance(lhs, rhs) < EPSILON) {
		result = true;
	}

	return result;
}

void Simulator::collideCells(Cell &lhs, Cell &rhs) {
	float cellCenterDistance = distance(lhs.position, rhs.position);
	if (cellCenterDistance < lhs.radius + rhs.radius + EPSILON) {
		// The two cells are colliding. Determine who eats who.
		Cell *preyCell = NULL;
		Cell *hunterCell = NULL;
		if (lhs.radius < rhs.radius) {
			preyCell = &lhs;
			hunterCell = &rhs;
		} else {
			preyCell = &rhs;
			hunterCell = &lhs;
		}

		// We want the hunter and prey to be just incident after the bite and preserve the cell matter.
		// Solve the system of equations.
		// | 0 < hunter radius < new hunter radius
		// | 0 <= new prey radius < prey radius
		// | new hunter raduis + new prey radius = distance(hunter center, prey center)
		// | hunter area + prey area = new hunter area + new prey area

		float newPreyRadius = 0.5f * (cellCenterDistance - sqrtf(2.0f * (hunterCell->radius * hunterCell->radius + preyCell->radius * preyCell->radius) - cellCenterDistance * cellCenterDistance)); 
		float newHunterRadius = cellCenterDistance - newPreyRadius - EPSILON;
		
		float oldHunterArea = hunterCell->getArea();
		float newHunterArea = PI * newHunterRadius * newHunterRadius;

		float biteArea = newHunterArea - oldHunterArea;

		preyCell->radius = newPreyRadius;
		hunterCell->radius = newHunterRadius;
		hunterCell->velocity = (oldHunterArea * hunterCell->velocity + biteArea * preyCell->velocity) / newHunterArea;
	}
}

void Simulator::defragmentCellPartitions(int deadCellIndex) {
	if (deadCellIndex < livePlayerCellCount) {
		
		// Reaplace the dead player cell with the last living one
		int lastLivingPlayerCellIndex = livePlayerCellCount - 1;
		if (deadCellIndex < lastLivingPlayerCellIndex) {
			cells[deadCellIndex] = cells[lastLivingPlayerCellIndex];
			playerCellInfo[deadCellIndex] = playerCellInfo[lastLivingPlayerCellIndex];
			playerCellInfo[deadCellIndex].cellIndex = deadCellIndex;
		}

		deadCellIndex = lastLivingPlayerCellIndex;

		--livePlayerCellCount;
	}

	// Replace the dead cell with the last living one
	int lastLivingCellIndex = liveCellCount - 1;
	if (deadCellIndex < lastLivingCellIndex) {
		cells[deadCellIndex] = cells[lastLivingCellIndex];
		cells[lastLivingCellIndex].radius = 0.0f;
	}

	--liveCellCount;
}
