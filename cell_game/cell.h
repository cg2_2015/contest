/*
	Copyright (C) 2014 Chaos Software

	Distributed under the Boost Software License, Version 1.0.
	See accompanying file LICENSE_1_0.txt or copy at
	http://www.boost.org/LICENSE_1_0.txt.
*/

#ifndef __CELL_H__
#define __CELL_H__

// Project headers
#include "vector2d.h"

namespace chaos {

class Cell {

public:
	float radius;
	Vector position;
	Vector velocity;

	Cell();

	Cell(float cellRadius, const Vector &cellPosition, const Vector &cellVelocity);

	bool isDead() const;

	float getArea() const;
	void setArea(const float newArea);
};

}; // namespace chaos

#endif // __CELL_H__
