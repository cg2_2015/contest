/*
	Copyright (C) 2014 Chaos Software

	Distributed under the Boost Software License, Version 1.0.
	See accompanying file LICENSE_1_0.txt or copy at
	http://www.boost.org/LICENSE_1_0.txt.
*/

// Project headers
#include "cell.h"
#include "math_utils.h"

using namespace std;
using namespace chaos;

////////////////////////////////////////////////////////////
// Cell implementation

Cell::Cell()
	: radius(0.0f), position(), velocity() {
}

Cell::Cell(float cellRadius, const Vector &cellPosition, const Vector &cellVelocity) 
	: radius(cellRadius), position(cellPosition), velocity(cellVelocity) {
}

bool Cell::isDead() const {
	return radius < EPSILON;
}

float Cell::getArea() const {
	return PI * radius * radius;
}

void Cell::setArea(const float newArea) {
	radius = sqrtf(INVERSE_PI * chaos::max(newArea, 0.0f));
}