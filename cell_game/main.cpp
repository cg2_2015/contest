/*
	Copyright (C) 2014 Chaos Software

	Distributed under the Boost Software License, Version 1.0.
	See accompanying file LICENSE_1_0.txt or copy at
	http://www.boost.org/LICENSE_1_0.txt.
*/

// Project headers
#include "cell.h"
#include "cell_ai.h"
#include "settings.h"
#include "simulator.h"
#include "math_utils.h"
#include "player_cell_info.h"

// GLUT - include last because of a redefinition problem
#include "glut.h"

using namespace std;
using namespace chaos;

Simulator simulator;

inline int getCircleSegmentCount(float radius) {
	static const float MAX_LINE_LENGTH = 0.01f;
	return clamp(DOUBLE_PI * radius / MAX_LINE_LENGTH, 8.0f, 128.0f);
}

void drawCircle(const Vector &center, float radius, bool fillCircle) {
	int circleSegmentCount = getCircleSegmentCount(radius);

	float stepAngleRadian = DOUBLE_PI / circleSegmentCount;
	float stepCos = cosf(stepAngleRadian);
	float stepSin = sinf(stepAngleRadian);

	float x = radius;
	float y = 0.0f;

	glBegin(fillCircle ? GL_TRIANGLE_FAN : GL_LINE_LOOP);
	while (0 < circleSegmentCount) {
		glVertex2f(center.x + x, center.y + y);

		float rotatedX = stepCos * x - stepSin * y;
		float rotatedY = stepSin * x + stepCos * y;
		x = rotatedX;
		y = rotatedY;

		--circleSegmentCount;
	}
	glEnd();
}

void display() {
	// Clear buffer
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	// Draw arena
	glColor3f(1.0f, 1.0f, 1.0f);
	glLineWidth(2.0f);
	drawCircle(simulator.settings.arenaCenter, simulator.settings.arenaRadius, false);

	// Get simulator info
	int playerCount = simulator.getLivePlayerCellCount();
	const PlayerCellInfo *playerCellInfo = simulator.getPlayerCellInfo();
	
	int cellCount = simulator.getLiveCellCount();
	const Cell *cells = simulator.getCells();
	
	// Draw players	
	for (int playerIndex = 0; playerIndex < playerCount; ++playerIndex) {
		const PlayerCellInfo &playerInfo = playerCellInfo[playerIndex];
		glColor3f(playerInfo.r, playerInfo.g, playerInfo.b);

		const Cell &playerCell = cells[playerInfo.cellIndex];
		drawCircle(playerCell.position, playerCell.radius, true);
	}

	// Draw NPC cells
	for (int cellIndex = playerCount; cellIndex < cellCount; ++cellIndex) {
		const Cell &cell = cells[cellIndex];

		if (playerCount == 1) {
			const Cell &playerOne = cells[0];
			if (cell.radius < playerOne.radius) {
				glColor3f(0.0f, 1.0f, 0.0f);
			} else {
				glColor3f(1.0f, 0.0f, 0.0f);
			}
		} else {
			glColor3f(0.5f, 0.5f, 0.5f);
		}

		drawCircle(cell.position, cell.radius, true);
	}

	// Swap buffers
	glutSwapBuffers();
}

void reshape(int width, int height) {
	int minDimension = chaos::min(width, height);
	glViewport((width - minDimension) / 2, (height - minDimension) / 2, minDimension, minDimension);
}

void idle() {
	static int guiTicks = 0;

	const Simulator::State simulatorState = simulator.getState();
	switch (simulatorState) {
		case Simulator::READY: {
			++guiTicks;
			simulator.tick();
			glutPostRedisplay();
			break;
		}
		case Simulator::VICTORY:
		case Simulator::FAILURE: {
			printf("Simulation ticks: %d\n", guiTicks);
			if (simulator.settings.exitOnSimulationFinished) {
				exit(0);
			}
			break;
		}
		default: {
			break; // silence clang warnings
		}
	}
}

void keyboardNormal(unsigned char key, int x, int y) {
	static const unsigned char KEYBOARD_ESCAPE_KEY = 27;
	static const unsigned char KEYBOARD_SPACE_KEY = ' ';

	switch (key) {
		case KEYBOARD_ESCAPE_KEY: {
			exit(0);
			break;
		}
		case KEYBOARD_SPACE_KEY: {
			simulator.toggleSimulationPause();
			break;
		}
	}
}

void keyboardSpecial(int key, int x, int y) {
	switch (key) {
		case GLUT_KEY_RIGHT: {
			if (simulator.getState() == Simulator::PAUSED) {
				simulator.tick();
				glutPostRedisplay();
			}
			break;
		}
	}
}

int main(int argc, char **argv) {
	// Initialize GLUT
	glutInit(&argc, argv);

	// Setup window
	glutInitWindowPosition((glutGet(GLUT_SCREEN_WIDTH)  - simulator.settings.displayResolution) / 2,
                           (glutGet(GLUT_SCREEN_HEIGHT) - simulator.settings.displayResolution) / 2);
	glutInitWindowSize(simulator.settings.displayResolution, simulator.settings.displayResolution);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);

	// Create window
	glutCreateWindow("CellGame");

	// Register callbacks
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboardNormal);
	glutSpecialFunc(keyboardSpecial);
	glutIdleFunc(idle);

	// Test settings
	int gameIndex = 0;
	int gameCount = 1;
	bool runGUI = false;	

	// Parse command line
	switch (argc) {
		case 1: {
			gameIndex = 0;
			gameCount = 1;
			runGUI = true;
			break;
		}
		case 2: {
			gameIndex = atoi(argv[1]);
			gameCount = 1;
			runGUI = true;
			break;
		}
		case 3: {
			gameIndex = 0;
			gameCount = atoi(argv[1]);
			runGUI = false;
			break;
		}
		default: {
			break;
		}
	}

	// Create the custom AI
	DefaultAI defaultAI = DefaultAI();

	if (runGUI) {
		simulator.settings.levelSeed = gameIndex;

		// Populate the level for the simulation
		simulator.populate();

		// Prepare and attach the custom AI
		defaultAI.prepare();
		simulator.addPlayerCell(&defaultAI, 0.0f, 1.0f, 1.0f);
		
		// Enter GLUT's main loop
		glutMainLoop();
	} else {
		const int TIME_LIMIT_IN_MINUTES = 10;
		const int TIME_LIMIT_IN_SECONDS = 60 * TIME_LIMIT_IN_MINUTES;
		const int CYCLES_PER_SECOND = 120;
		const int CYCLE_LIMIT = CYCLES_PER_SECOND * TIME_LIMIT_IN_SECONDS;

		int wins = 0;
		int ties = 0;
		int losses = 0;
		int fastestGame = 0;
		int fastestGameCycles = CYCLE_LIMIT;
		int slowestGame = 0;
		int slowestGameCycles = 0;

		for (int gameIndex = 0; gameIndex < gameCount; ++gameIndex) {
			Simulator gameSimulator;

			gameSimulator.settings.levelSeed = gameIndex;

			// Populate the level for the simulation
			gameSimulator.populate();

			// Prepare and attach the custom AI
			defaultAI.prepare();
			gameSimulator.addPlayerCell(&defaultAI, 0.0f, 1.0f, 1.0f);
			
			printf("Game %d ", gameIndex);

			int cycleBudget = CYCLE_LIMIT;
			while (!gameSimulator.isFinished() && (0 < cycleBudget)) {
				gameSimulator.tick();
				--cycleBudget;
			}

			int cycleCount = CYCLE_LIMIT - cycleBudget;

			Simulator::State state = gameSimulator.getState();
			switch (state) {
				case Simulator::READY: {
					++ties;
					printf("Timed out at %d cycles\n", cycleCount);
					break;
				}
				case Simulator::VICTORY: {
					++wins;
					
					if (cycleCount < fastestGameCycles) {
						fastestGameCycles = cycleCount;
						fastestGame = gameIndex;
					}

					if (slowestGameCycles < cycleCount) {
						slowestGameCycles = cycleCount;
						slowestGame = gameIndex;
					}

					printf("Won in %d cycles\n", cycleCount);
					break;
				}
				case Simulator::FAILURE: {
					++losses;
					printf("Lost in %d cycles\n", cycleCount);
					break;
				}
				default : {
					break;
				}
			}
		}

		printf("\n");
		printf("Won games: %d\n", wins);
		printf("Tie games: %d\n", ties);
		printf("Lost games: %d\n", losses);
		printf("Fastest game: %d\n", fastestGame);
		printf("Slowest game: %d\n", slowestGame);
	}

	return 0;
}
