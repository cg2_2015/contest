/*
	Copyright (C) 2014 Chaos Software

	Distributed under the Boost Software License, Version 1.0.
	See accompanying file LICENSE_1_0.txt or copy at
	http://www.boost.org/LICENSE_1_0.txt.
*/

// Project headers
#include "cell.h"
#include "cell_ai.h"
#include "math_utils.h"

using namespace std;
using namespace chaos;

////////////////////////////////////////////////////////////
// ICellAI implementation

ICellAI::~ICellAI() {
}

void ICellAI::prepare() {
}

////////////////////////////////////////////////////////////
// DefaultAI implementation

DefaultAI::~DefaultAI() {
}


void DefaultAI::prepare() {
}

Vector DefaultAI::calculateAcceleration(const Cell *cells, const int liveCellCount) const {
	Vector acceleration;
	return acceleration;
}
