/*
	Copyright (C) 2014 Chaos Software

	Distributed under the Boost Software License, Version 1.0.
	See accompanying file LICENSE_1_0.txt or copy at
	http://www.boost.org/LICENSE_1_0.txt.
*/

#ifndef __MATH_UTILS_H__
#define __MATH_UTILS_H__

// Standard headers // RAND_MAX, rand
#include <cstdlib>

// Project headers
#include "cell.h"
#include "vector2d.h"

namespace chaos {

const float EPSILON = 1e-6f;
const float LARGE_FLOAT = 1.0f / EPSILON;

const float PI = 3.1415927f;
const float DOUBLE_PI = 6.2831853f;
const float INVERSE_PI = 0.31830986f;

inline float getFloatSample(const float minValue, const float maxValue, unsigned short int xsubi[3]) {
	return minValue + (maxValue - minValue) * erand48(xsubi);
}

inline Vector getRandomDirection(unsigned short int xsubi[3]) {
	float theta = getFloatSample(0.0f, DOUBLE_PI, xsubi);
	return Vector(cosf(theta), sinf(theta));
}

inline Vector getDiskSample(float diskRadius, unsigned short int xsubi[3]) {
	float radius = getFloatSample(0.0f, diskRadius, xsubi);
	return sqrtf(radius) * getRandomDirection(xsubi);
}

inline float distance(const Vector &rhs, const Vector &lhs) {
	Vector difference = rhs - lhs;
	return difference.length();
}

inline float distance(const Cell &cell, const Vector &position) {
	float result = LARGE_FLOAT;
	
	if (!cell.isDead()) {
		result = distance(cell.position, position) - cell.radius;
	}

	return result;
}

inline float distance(const Vector &position, const Cell &cell) {
	return distance(cell, position);
}

inline float distance(const Cell &lhs, const Cell &rhs) {
	float result = LARGE_FLOAT;

	if (!lhs.isDead() && !rhs.isDead()) {
		result = distance(lhs.position, rhs.position) - (lhs.radius + rhs.radius);
	}

	return result;
}

inline float dot(const Vector &rhs, const Vector &lhs) {
	return rhs.x * lhs.x + rhs.y * lhs.y;
}

template <typename T>
inline T min(const T &lhs, const T &rhs) {
	return (lhs < rhs) ? lhs : rhs;
}

template <typename T>
inline T max(const T &lhs, const T &rhs) {
	return (lhs < rhs) ? rhs : lhs;
}

template <typename T>
inline T clamp(const T &value, const T &lowerLimit, const T &upperLimit) {
	return min(max(value, lowerLimit), upperLimit);
}

}; // namespace chaos

#endif // __MATH_UTILS_H__
