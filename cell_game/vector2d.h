/*
	Copyright (C) 2014 Chaos Software

	Distributed under the Boost Software License, Version 1.0.
	See accompanying file LICENSE_1_0.txt or copy at
	http://www.boost.org/LICENSE_1_0.txt.
*/

#ifndef __VECTOR2D_H__
#define __VECTOR2D_H__

// Standard headers
#include <cmath> // sqrtf


namespace chaos {

class Vector {

public:
	float x;
	float y;

	Vector();

	Vector(const float inputX, const float inputY);

	float length() const;

	void normalize();

	Vector operator-() const;

	Vector operator+(const float rhs) const;

	Vector operator+(const Vector &rhs) const;

	Vector operator-(const float rhs) const;

	Vector operator-(const Vector &rhs) const;

	Vector operator*(const float rhs) const;

	Vector operator*(const Vector &rhs) const;

	Vector operator/(const float rhs) const;

	Vector operator/(const Vector &rhs) const;

	Vector& operator+=(const float rhs);

	Vector& operator+=(const Vector &rhs);

	Vector& operator-=(const float rhs);

	Vector& operator-=(const Vector &rhs);

	Vector& operator*=(const float rhs);

	Vector& operator*=(const Vector &rhs);

	Vector& operator/=(const float rhs);

	Vector& operator/=(const Vector &rhs);
};

inline Vector::Vector()
	: x(0.0f), y(0.0f) {
}

inline Vector::Vector(const float inputX, const float inputY) 
	: x(inputX), y(inputY) {
}

inline float Vector::length() const {
	return sqrtf(x * x + y * y);
}

inline void Vector::normalize() {
	float vectorLength = length();
	if (1e-6f < vectorLength) {
		x /= vectorLength;
		y /= vectorLength;
	}
}

inline Vector Vector::operator-() const {
	return Vector(-x, -y);
}

inline Vector Vector::operator+(const float rhs) const {
	return Vector(x + rhs, y + rhs);
}

inline Vector Vector::operator+(const Vector &rhs) const {
	return Vector(x + rhs.x, y + rhs.y);
}

inline Vector operator+(const float lhs, const Vector &rhs) {
	return Vector(lhs + rhs.x, lhs + rhs.y);
}

inline Vector Vector::operator-(const float rhs) const {
	return Vector(x - rhs, y - rhs);
}

inline Vector Vector::operator-(const Vector &rhs) const {
	return Vector(x - rhs.x, y - rhs.y);
}

inline Vector operator-(const float lhs, const Vector &rhs) {
	return Vector(lhs - rhs.x, lhs - rhs.y);
}

inline Vector Vector::operator*(const float rhs) const {
	return Vector(x * rhs, y * rhs);
}

inline Vector Vector::operator*(const Vector &rhs) const {
	return Vector(x * rhs.x, y * rhs.y);
}

inline Vector operator*(const float lhs, const Vector &rhs) {
	return Vector(lhs * rhs.x, lhs * rhs.y);
}

inline Vector Vector::operator/(const float rhs) const {
	return Vector(x / rhs, y / rhs);
}

inline Vector Vector::operator/(const Vector &rhs) const {
	return Vector(x / rhs.x, y / rhs.y);
}

inline Vector operator/(const float lhs, const Vector &rhs) {
	return Vector(lhs / rhs.x, lhs / rhs.y);
}

inline Vector& Vector::operator+=(const float rhs) {
	x += rhs;
	y += rhs;
	return *this;
}

inline Vector& Vector::operator+=(const Vector &rhs) {
	x += rhs.x;
	y += rhs.y;
	return *this;
}

inline Vector& Vector::operator-=(const float rhs) {
	x -= rhs;
	y -= rhs;
	return *this;
}

inline Vector& Vector::operator-=(const Vector &rhs) {
	x -= rhs.x;
	y -= rhs.y;
	return *this;
}

inline Vector& Vector::operator*=(const float rhs) {
	x *= rhs;
	y *= rhs;
	return *this;
}

inline Vector& Vector::operator*=(const Vector &rhs) {
	x *= rhs.x;
	y *= rhs.y;
	return *this;
}

inline Vector& Vector::operator/=(const float rhs) {
	x /= rhs;
	y /= rhs;
	return *this;
}

inline Vector& Vector::operator/=(const Vector &rhs) {
	x /= rhs.x;
	y /= rhs.y;
	return *this;
}

}; // namespace chaos

#endif // __VECTOR2D_H__
