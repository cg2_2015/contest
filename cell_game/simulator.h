/*
	Copyright (C) 2014 Chaos Software

	Distributed under the Boost Software License, Version 1.0.
	See accompanying file LICENSE_1_0.txt or copy at
	http://www.boost.org/LICENSE_1_0.txt.
*/

#ifndef __SIMULATOR_H__
#define __SIMULATOR_H__

namespace chaos {

// Project headers
#include "settings.h"

class Cell;
class ICellAI;
class PlayerCellInfo;

class Simulator {

public:
	enum State {
		NOT_READY,
		READY,
		PAUSED,
		VICTORY,
		FAILURE
	};

	Settings settings;

	Simulator();
	~Simulator();

	State getState() const;

	bool isFinished() const;

	int getLiveCellCount() const;
	const Cell* getCells() const;

	int getLivePlayerCellCount() const;
	const PlayerCellInfo* getPlayerCellInfo() const;

	void toggleSimulationPause();

	void populate();

	void addPlayerCell(const ICellAI *cellAI, const float r, const float g, const float b);

	void tick();

	void reset();

private:
	State state;

	Cell *cells;
	Cell *playerViewCells;
	int liveCellCount;

	PlayerCellInfo *playerCellInfo;
	int livePlayerCellCount;

	// Forbid copying
	Simulator(const Simulator &other);
	Simulator& operator=(const Simulator &other);

	void acceleratePlayerCell(Cell &cell, const ICellAI *cellAI);

	void advanceCell(Cell &cell);

	bool isCellCollidingWithArena(const Cell &cell) const;
	void collideCellWithArena(Cell &cell);

	bool areCellsColliding(const Cell &lhs, const Cell &rhs) const;
	void collideCells(Cell &lhs, Cell &rhs);

	void defragmentCellPartitions(int deadCellIndex);
};

}; // namespace chaos

#endif // __SIMULATOR_H__