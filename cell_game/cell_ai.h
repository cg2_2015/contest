/*
	Copyright (C) 2014 Chaos Software

	Distributed under the Boost Software License, Version 1.0.
	See accompanying file LICENSE_1_0.txt or copy at
	http://www.boost.org/LICENSE_1_0.txt.
*/

#ifndef __CELL_AI_H__
#define __CELL_AI_H__

// Project headers
#include "vector2d.h"

namespace chaos {

class Cell;

////////////////////////////////////////////////////////////
// ICellAI interface declaration

class ICellAI {

public:
	virtual ~ICellAI();

	virtual void prepare();

	virtual Vector calculateAcceleration(const Cell *cells, const int liveCellCount) const = 0;
};

////////////////////////////////////////////////////////////
// DefaultAI declaration

class DefaultAI : public ICellAI {

public:
	virtual ~DefaultAI();

	virtual void prepare();

	virtual Vector calculateAcceleration(const Cell *cells, const int liveCellCount) const;
};

}; // namespace chaos

#endif // __CELL_AI_H__