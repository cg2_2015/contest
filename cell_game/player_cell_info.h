/*
	Copyright (C) 2014 Chaos Software

	Distributed under the Boost Software License, Version 1.0.
	See accompanying file LICENSE_1_0.txt or copy at
	http://www.boost.org/LICENSE_1_0.txt.
*/

#ifndef __PLAYER_CELL_INFO_H__
#define __PLAYER_CELL_INFO_H__

// Standard headers
#include <cstdlib> // NULL

namespace chaos {

class ICellAI;

class PlayerCellInfo {

public:
	int cellIndex;
	const ICellAI *ai;
	float r;
	float g;
	float b;

	PlayerCellInfo()
		: cellIndex(-1), ai(NULL), r(0.0f), g(0.0f), b(0.0f) {
	}
};

} // namespace chaos

#endif // __PLAYER_CELL_INFO_H__