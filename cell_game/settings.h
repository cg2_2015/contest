/*
	Copyright (C) 2014 Chaos Software

	Distributed under the Boost Software License, Version 1.0.
	See accompanying file LICENSE_1_0.txt or copy at
	http://www.boost.org/LICENSE_1_0.txt.
*/

#ifndef __SETTINGS_H__
#define __SETTINGS_H__

// Standard headers
#include <cstdio> // FILE, fopen, fclose, fscanf
#include <cstring> // strncpy

// Project headers
#include "vector2d.h"

#ifndef MAX_PATH
	#define MAX_PATH 256
#endif // MAX_PATH

using std::fopen;
using std::fclose;
using std::fscanf;
using std::strncpy;

namespace chaos {

class Settings {

public:
	// Test parameters. Frequently changed. Overloaded by command line.
	int levelSeed;
	int cellCount;

	// Simulation parameters. Usually don't change.
	float cellMinimumRadius;
	float cellMaximumRadius;
	float cellMaximumVelocity;
	float playerCellInitialRadius;
	int exitOnSimulationFinished;

	// System parameters. Almost never change.
	int playerCellCount;
	Vector arenaCenter;
	float arenaRadius;
	float tickLength;
	int displayResolution;
	char settingsFilePath[MAX_PATH];

	Settings() 
		: levelSeed(0)
		, cellCount(64)

		, cellMinimumRadius(0.005f)
		, cellMaximumRadius(0.02f)
		, cellMaximumVelocity(0.25f)
		, playerCellInitialRadius(0.01f)
		, exitOnSimulationFinished(1)

		, playerCellCount(1)
		, arenaCenter(0.0f, 0.0f)
		, arenaRadius(1.0f)
		, tickLength(0.005f)
		, displayResolution(640) {

		strncpy(settingsFilePath, ".\\settings.txt\0", sizeof(settingsFilePath));
	}

	void load() {
		FILE *settingsFile = fopen(settingsFilePath, "r");
		if (settingsFile) {
			fscanf(settingsFile, "%*s %d", &levelSeed);
			fscanf(settingsFile, "%*s %d", &cellCount);
			
			fscanf(settingsFile, "%*s %f", &cellMinimumRadius);
			fscanf(settingsFile, "%*s %f", &cellMaximumRadius);
			fscanf(settingsFile, "%*s %f", &cellMaximumVelocity);
			fscanf(settingsFile, "%*s %f", &playerCellInitialRadius);
			fscanf(settingsFile, "%*s %d", &exitOnSimulationFinished);

			fscanf(settingsFile, "%*s %d", &playerCellCount);
			fscanf(settingsFile, "%*s %f %f", &arenaCenter.x, &arenaCenter.y);
			fscanf(settingsFile, "%*s %f", &arenaRadius);
			fscanf(settingsFile, "%*s %f", &tickLength);
			fscanf(settingsFile, "%*s %d", &displayResolution);

			playerCellCount = (playerCellCount < cellCount) ? playerCellCount : cellCount;

			fclose(settingsFile);
		}
	}
};

}; // namespace chaos

#endif // __SETTINGS_H__